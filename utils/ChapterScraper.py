from requests import get
from bs4 import BeautifulSoup
#from os import linesep
#from re import sub

class Chapter:
    """
    Represents a chapter.
    """

    def __init__(
        self,
        number: int,
        content: str,
        next_chapter: str, 
    ):
        self.number = number
        self.next_chapter = next_chapter
        self.content = content

    def __str__(self):
        return f"Chapter {self.number}: {len(self.content)}"


class ChapterDataScraper:
    BASE_URL = "https://boxnovel.com/novel/"

    def __init__(self) -> None:
        self.content = None
        self.chapter = None

    def get_chapter(self, novel_name: str, chapter: str):
        """
        Returns metadata for a given novel.
        """
        header = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
            "referer": "https://www.google.com/",
        }

        url = f"{self.BASE_URL}/{self.sanitize_name(novel_name)}/chapter-{chapter}/"
        print(f"{novel_name.title()} - {chapter}")

        response = get(url, headers=header)
        if response.status_code == 200:
            self.content = response.content
            self.boxnovel_metadata()
        else:
            return None

        return self.chapter

    def boxnovel_metadata(self):
        """
        Extracts data from the given content.
        """
        soup = BeautifulSoup(self.content, "html.parser")
        try:
            next_chapter = soup.find("div", {"class": "nav-next"}).findChild('a')['href']
        except:
            next_chapter = None
        try:
            chapter_data = soup.find("div", {"class": "reading-content"}).text
        except:
            return None

        #chapter_data = sub(r'\n\s*\n', '\n\n', chapter_data)

        #chapter_data = linesep.join([s for s in chapter_data.splitlines() if s])

        title = soup.find("title").text.split("-")[1].lstrip()
        chapter_number = title.split(" ")[1]
        chapter_number = int(chapter_number)
        if "END" in title:
            chapter_number += 1

        chapter = Chapter(content=chapter_data, number=chapter_number,
            next_chapter=next_chapter)   

        self.chapter = chapter         

    def sanitize_name(self, name: str):
        """
        Returns a sanitized version of the given name.
        """
        return "-".join(name.split()).lower()

