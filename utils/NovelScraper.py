from requests import get
from bs4 import BeautifulSoup


class Novel:
    """
    Represents a novel.
    """

    def __init__(
        self,
        name: str,
        author: str,
        genre: list,
        tag: str,
        release: int,
        status: str,
        cover: str,
    ):

        self.name = name
        self.author = author
        self.genre = genre
        self.tag = tag
        self.release = release
        self.status = status
        self.cover = cover
        self.latest_chapter = None
        self.publisher = None

    def __str__(self):
        return f"{self.name} by {self.author} in {self.release}"


class NovelDataScraper:
    BASE_URL = "https://boxnovel.com/novel/"
    EXTRA_URL = "https://comrademao.com/novel/"

    def __init__(self) -> None:
        self.content = None
        self.novel = None

    def get_metadata(self, novel_name: str):
        """
        Returns metadata for a given novel.
        """
        header = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
            "referer": "https://www.google.com/",
        }

        url = f"{self.BASE_URL}/{self.sanitize_name(novel_name)}/"
        print(f"Boxnovel- {url}")
        response = get(url, headers=header)
        if response.status_code == 200:
            self.content = response.content
            self.boxnovel_metadata()
        else:
            return None

        url = f"{self.EXTRA_URL}{self.sanitize_name(novel_name)}"
        print(f"ComradeMao- {url}")

        response = get(url, headers=header)

        if response.status_code == 200:
            self.content = response.content
            self.comrademao_metadata()

        return self.novel

    def boxnovel_metadata(self):
        """
        Extracts data from the given content.
        """
        soup = BeautifulSoup(self.content, "html.parser")

        title = soup.find("title").text.split("–")[0].strip()

        if "Manga" in title:
            return Novel()

        author = (
            soup.find("div", {"class": "author-content"})
            .text.split(",")[0]
            .replace("\n", "")
        )

        cover = soup.find("div", {"class": "summary_image"})

        print(cover)
        
        cover = cover.findChild("img")["data-src"]


        genre = soup.find("div", {"class": "genres-content"}).text
        genre = [gen.strip().replace("\n", "").title() for gen in genre.split(",")]

        tag = soup.find("div", {"class": "tags-content"})
        tag = tag.text.strip().replace("\n", "").title()

        data = soup.find_all("div", {"class": "summary-content"})

        if len(data) > 7:
            if len(data) == 8:
                release, status = -1, data[7].text.strip()
            else:
                release, status = data[7].text.strip(), data[8].text.strip()
        else:
            release = -1
            status = data[6].text.strip()

        novel = Novel(
            name=title,
            author=author,
            genre=genre,
            tag=tag,
            release=int(release),
            status=status.title(),
            cover=cover,
        )
        print(novel)
        self.novel = novel

    def comrademao_metadata(self):
        soup = BeautifulSoup(self.content, "html.parser")

        try:
            latest_chapter = soup.select(
                "body  section  div:nth-child(2)  nav.level.is-mobile  div:nth-child(2)  div  p.title"
            )[0].text.strip()
            latest_chapter = int(latest_chapter)

        except:
            latest_chapter = -1

        try:
            publisher = soup.select("#NovelInfo  p:nth-child(4)  a")[0].text.strip()
        except:
            publisher = "Unknown"

        self.novel.latest_chapter = latest_chapter
        self.novel.publisher = publisher

    def sanitize_name(self, name: str):
        """
        Returns a sanitized version of the given name.
        """
        return "-".join(name.split()).lower()
