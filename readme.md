# NovelMetaData

Live on Glitch [here](https://novelmetadata.glitch.me/novel/reverend%20insanity)


## Features
- [x] Novel metadata
- [x] Cache requests
- [ ] Store completed novel metadata in a database
- [ ] Chapter data
### Running

    1) Have Python3 installed and added to path

    2) git clone https://BlackSmithOP@bitbucket.org/BlackSmithOP/novelmetadata.git

    3) cd novelmetadata 

    4) py -m pip install -r requirements.txt

    5) py app.py

### Endpoints

`/` - Index

`/novel/:novel_name` - Get Novel Metadata


#### Examples

 `/novel/Reincarnation Of The Strongest Sword God`

```json
{
    author: "Lucky Old Cat",
    cover: "https://boxnovel.com/wp-content/uploads/2018/10/Reincarnation-Of-The-Strongest-Sword-God-193x278.jpg",
    genre: [
        "Action",
        "Adventure",
        "Fantasy",
        "Martial Arts",
        "Xuanhuan"
    ],
    name: "Reincarnation Of The Strongest Sword God",
    release: 2015,
    status: "Completed",
    tag: "Chinese Novel, Completed"
}
```

Append a `/view/` to the request to get a rendered view of the data

<img src="https://i.ibb.co/V2DWjrc/image.png" alt="Screenshot" width="200"/>

