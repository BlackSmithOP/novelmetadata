from flask import Flask, jsonify, render_template
from flask_caching import Cache
import os
from utils import NovelScraper, ChapterScraper

config = {
    "DEBUG": True,  # some Flask specific configs
    "CACHE_TYPE": "SimpleCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300,
}

app = Flask(__name__)
app.secret_key = os.urandom(12)

app.config.from_mapping(config)
cache = Cache(app)

novel_scraper = NovelScraper.NovelDataScraper()
chapter_scraper = ChapterScraper.ChapterDataScraper()


@app.route("/", methods=["GET"])
@cache.cached()
def home():
    return "✔ App running"


@app.route("/novel/<novel_name>/<render>/", methods=["GET"])
@app.route("/novel/<novel_name>/", methods=["GET"])
@cache.memoize()
def novel(novel_name, render=None):

    data = novel_scraper.get_metadata(novel_name=novel_name)
    print(data)

    if render != None and render == "view":
        return render_template("novel.html", data=data)

    elif data != None:
        if bool(data.__dict__):
            return jsonify(data.__dict__), 200
    else:
        return {"error": "No data found for this novel."}

@app.route("/chapter/<novel_name>/<chapter>", methods=["GET"])
@app.route("/chapter/<novel_name>/<chapter>/<render>", methods=["GET"])
@cache.memoize()
def chapter(novel_name,chapter, render=None):

    data = chapter_scraper.get_chapter(novel_name=novel_name, chapter=chapter)    

    if render != None and render == "view":

        content = data.content
        #content = content.split("\n")
        #print(content)
        #content = [f"<p>{i}</p>" for i in content if i]
        #content = "".join(content)
        return render_template("chapter.html", content=content, name=novel_name.title(),
        num = data.number)

    elif data != None:
        if bool(data.__dict__):
            return jsonify(data.__dict__), 200
    else:
        return {"error": "No data found for this novel."}

# test reCaptcha
@app.route("/captcha", methods=["GET"])
def captcha():
    return render_template("reCaptcha.html")


if __name__ == "__main__":
    cache.clear()
    app.run(debug=True, use_reloader=True)
